﻿using System;
using Library;

namespace Application
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tool box content");
            Console.WriteLine($"Amount of nails: {ToolBox.AmountOfNails}");
            Console.WriteLine($"Has hammer?: {ToolBox.HasHammer}");
            Console.ReadKey();
        }
    }
}
