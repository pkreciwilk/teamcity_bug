﻿using Library;
using NUnit.Framework;

namespace LibraryUnitTests
{
    [TestFixture]
    public class ToolBoxTests
    {
        [Test]
        public void CheckAmountOfNails()
        {
            Assert.AreEqual(20, ToolBox.AmountOfNails);
        }
    }
}
